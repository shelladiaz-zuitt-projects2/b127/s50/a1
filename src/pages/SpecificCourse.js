import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, NavItem } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Link, useHistory, useParams } from 'react-router-dom';

import UserContext from '../UserContext';

export default function SpecificCourse(){

    const { user } = useContext(UserContext);

    const[ name, setName ] = useState('')
    const [ description, setDescription ] = useState('')
    const [ price, setPrice ] = useState(0)

    //useparams() = contains any values we are trying to pass in the URL stored in a key/value pair
    //useParams is how we received the courseId passed via URL
    const { courseId } = useParams();
    // console.log(courseId);

    useEffect(() => {
        fetch(`http://localhost:4000/courses/${ courseId }`)
        .then(res => res.json())
        .then(data => {
            setName(data.name)
            setDescription(data.description)
            setPrice(data.price)
        })
    }, [])

    const enroll = (courseId) => {
        fetch(`http://localhost:4000/users/enroll`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                courseId: courseId
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                   title: "Yeeeey",
                   icon: 'success' ,
                   text: `You have successfully enrolled for this ${name}`
                })
            }else{
                Swal.fire({
                    title: "ops",
                    icon: 'error' ,
                    text: `try again`
                 })
            }
        })
    }

    return(
        <Container>
            <Card>
                <Card.Header className="bg-dark text-white text-center pb-0">
                    <h4>{name}</h4>
                </Card.Header>
                <Card.Body>
                    <Card.Text>
                        {description}
                    </Card.Text>
                    <h6>Php {price}</h6>
                </Card.Body>
                <Card.Footer>
                {
                    user.accesToken !== null ?
                        <Button variant="primary" block onClick={() => enroll(courseId)}> Enroll </Button>
                        :
                        <Link className="btn btn-warning btn-block" to="/login">Log in to Enroll</Link>

                }
                </Card.Footer>
            </Card>
        </Container>
    )
}