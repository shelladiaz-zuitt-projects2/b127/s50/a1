//use for mock data base
// import { Fragment  } from 'react';
// import courseData from '../data/courseData';
// import CourseCard from '../components/CourseCard';
 //use for mock data base
/**export default function Courses(){
    //checls to see if the mock data was captured
    console.log(courseData)
    console.log(courseData[0])

    //para madispay lahat ng courses gagamit ng map method.
    //map() method loops through the individual course objects in our array and returns a component for each course

    const courses = courseData.map(course => {
        return(
            //naglagay ng key para maging unique para walang error that help React js identify which /elements have been change, added or removed
            //to keep track the data of courses
            <CourseCard key={course.id} courseProp={course}/>
        )
    })

    return(
        <Fragment>
            <h1>Courses</h1>
            {courses}
        </Fragment>
  
    )
} */

//for live db
import { useState, useEffect, useContext } from "react";
//bootsrap
import { Container } from "react-bootstrap";
//components
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
//react context
import UserContext from "../UserContext";



export default function Courses() {
    const { user } = useContext(UserContext);

    const [allCourses, setAllCourses] = useState([])

    const fetchData = () => {
        fetch('http://localhost:4000/courses/all')
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setAllCourses(data)
        })
    }

    //1 time render
    useEffect(() => {
        fetchData()
    }, [])
    
    return(
        <Container>
            {
                (user.isAdmin === true) ?
                <AdminView coursesData={allCourses} fetchData={fetchData}/> //iniadd ung fetchdata para mag reload
                :
                <UserView coursesData={allCourses} />
            }
        </Container>

    )
}
