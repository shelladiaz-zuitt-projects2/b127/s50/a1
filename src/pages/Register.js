/**Activity */
import { Fragment, useEffect, useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Register(){

const history = useHistory();

const [firstName, setFirstName] = useState('');
const [lastName, setLastName] = useState('');
const [mobileNo, setMobileNo] = useState('');
const [email, setEmail] = useState('');
const [password, setPassword] = useState('');

const { user, setUser } = useContext(UserContext);
const [ isActive, setIsActive ] = useState(false);


useEffect(()=>{
    if((firstName !== '' && lastName !== '' && mobileNo !== "" && email !== '' &&  password !== '') && (mobileNo.length >= 11)){
        setIsActive(true);
    }else{
        setIsActive(false);
    }
}, [firstName, lastName, mobileNo, email, password])

    function registerUser(e){
        e.preventDefault();
        fetch('http://localhost:4000/users/checkEmail', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title: 'Opps!',
                    icon: 'error',
                    text: 'Email already exists'
                })

            }else{
                fetch('http://localhost:4000/users/register', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        mobileNo: mobileNo,
                        email: email,
                        password: password
                    })
                })
                .then(res => res.json())
                .then(data => {

                    if(data === true){
                        Swal.fire({
                            title: 'Yehey!',
                            icon: 'success',
                            text: 'Registration Successful!'
                        })
                        history.push("/login")
                    }else{
                        Swal.fire({
                            title: 'Something went wrong!',
                            icon: 'error',
                            text: 'Please try again!'
                        })

                    }
                    
                
                  
    
                })
                setFirstName('')
                setLastName('')
                setMobileNo('')
                setEmail('')
                setPassword('')

            }
        })

        
    }

    return(
        (user.accessToken !== null) ?
            <Redirect to="/"/>
            : 
        <Fragment>
            <h1>Register</h1>
            <Form onSubmit={(e) => registerUser(e)}>
                <Form.Group>
                    <Form.Label>
                        First Name:
                    </Form.Label>
                    <Form.Control 
                        type="text"
                        placeholder="Enter First Name"
                        value={firstName}
                        onChange={e => setFirstName(e.target.value)}
                        required

                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Last Name:
                    </Form.Label>
                    <Form.Control 
                        type="text"
                        placeholder="Enter Last Name"
                        value={lastName}
                        onChange={e => setLastName(e.target.value)}
                        required

                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Mobile Number:
                    </Form.Label>
                    <Form.Control 
                        type="text"
                        placeholder="Enter Mobile Number"
                        value={mobileNo}
                        onChange={e => setMobileNo(e.target.value)}
                        required

                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Email address:
                    </Form.Label>
                    <Form.Control 
                        type="email"
                        placeholder="Enter Email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required

                    />
                    <Form.Text className="text-muted">
                        We will never share your email with everyone else.
                    </Form.Text>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password:</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Enter Password"
                        onChange={e => setPassword(e.target.value)}
                        value={password}
                        required
                    />
                </Form.Group>
                { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Submit
                    </Button>
                }
       
               
            </Form>
        </Fragment>
        
    )
}





















/**
 * sample
 * 
 * import { Fragment, useEffect, useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Register(){

//useState. state hooks to store the values of the input fields
const [email, setEmail] = useState('');
const [password1, setPassword1] = useState('');
const [password2, setPassword2] = useState('');
const [isActive, setIsActive] = useState(false);
const { user } = useContext(UserContext);

console.log(email);
console.log(password1);
console.log(password2);

useEffect(()=>{
    //validation to enable submit button when all fields are populated and both passwords match
    if((email !== '' && password1 !== '' && password2 !== '')&& (password1===password2)){
        setIsActive(true);
    }else{
        setIsActive(false);
    }
}, [email, password1, password2])

    function registerUser(e){
        e.preventDefault();
       //to clear out the data in our input fields
        setEmail('')
        setPassword1('')
        setPassword2('')
        
        Swal.fire({
            title: 'Yaaaaaaaaaaaaaaaay!',
            icon: 'success',
            text: 'Thank you for registering!'
        })
         

        
    }

    return(
        (user.accessToken !== null) ?
            <Redirect to="/"/>
            : 
        <Fragment>
            <h1>Register</h1>
            <Form onSubmit={(e) => registerUser(e)}>
                <Form.Group>
                    <Form.Label>
                        Email address:
                    </Form.Label>
                    <Form.Control 
                        type="email"
                        placeholder="Enter Email"
                        value={email}
                        //the e.target.value property allows us to gain access to the input fields current value to be used when submitting form data
                        onChange={e => setEmail(e.target.value)}
                        required

                    />
                    <Form.Text className="text-muted">
                        We will never share your email with everyone else.
                    </Form.Text>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password:</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Enter your Password"
                        onChange={e => setPassword1(e.target.value)}
                        value={password1}
                        required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Verify Password:</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Verify your Password"
                        onChange={e => setPassword2(e.target.value)}
                        value={password2}
                        required
                    />
                </Form.Group>
                { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Submit
                    </Button>
                }
       
               
            </Form>
        </Fragment>
        
    )
}
 */