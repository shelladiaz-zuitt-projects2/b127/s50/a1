import { useContext, useEffect } from "react";
import { Redirect } from "react-router-dom";
import UserContext from "../UserContext";


export default function Logout(){

    const { setUser, unsetUser } = useContext(UserContext)

    //clear the localstorage of the user's information
    unsetUser();

    
    useEffect(() => {
        //set the user state back to its original value
        setUser({ accessToken: null })
    }, [])

    return(
        <Redirect to="/login"/>
    )
}