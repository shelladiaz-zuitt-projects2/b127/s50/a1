import { Fragment, useEffect, useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
//reactcontext
import UserContext from '../UserContext';
import { Redirect, useHistory } from 'react-router-dom'; 

export default function Login(){
    //the useHistory hooks gives you access to the history instance that you may use to navigate/ to access the location
    const history = useHistory();

    //userContext is a react hook used to unwrap our context. It will return the data passed as values by a provider(UserContext.PRovider component in App.js)
    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    console.log(email);
    console.log(password);

    useEffect(()=>{
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password])

    function loginUser(e){ 
        e.preventDefault();

        //fetch has 2 arg
        //1. URL from the server(routes)
        //2. optional object which contains additional information about our requests such as method, headers, body etc

        fetch(`${ process.env.REACT_APP_API_URL }/users/login`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email, //email is from useState getter
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data.accessToken !== undefined){
                localStorage.setItem('accessToken', data.accessToken);
                setUser({ accessToken: data.accessToken });

                Swal.fire({
                    title: 'Yaaaaaaaaaaaaaay!',
                    icon: 'success',
                    text: 'Thank you for loggin in here!'
                })

                //get users details from our token
                fetch('http://localhost:4000/users/details', {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    if(data.isAdmin === true){
                        localStorage.setItem('email', data.email)
                        localStorage.setItem('isAdmin', data.isAdmin)
                        setUser({
                            email: data.email,
                            isAdmin: data.isAdmin
                        })
                        //if admin, redirect the page to /courses
                        //push(path) - pushes a new entry onto the history stack
                        history.push('/courses')
                    }else{
                        //if not admin, redirect the page to homepage
                        history.push('/')
                    }

                })
            }else{
                Swal.fire({
                    title: 'Oooooooopssss!',
                    icon: 'error',
                    text: 'Something Went Wrong!'
                })
            }
            setEmail('')
            setPassword('')

        })
     
        /**
         * login
         *  Swal.fire({
            title: 'Yaaaaaaaaaaaaaaaay!',
            icon: 'success',
            text: 'Welcome!'
        })
        //local storage allows us to save data within our browsers as strings
        //the setItem() method of the storage interface, when passed a key name and value, will add that key to the given storage object or update the key's value if its already exists
        //setItem is used to store data in the localStorage as a string
        localStorage.setItem('email', email)
        setUser({ email: email })

        setEmail('')
        setPassword('')
         */
       
        
    }


    return(
        
    //create a conditional statement that will redirect the user to the homepage when a user is logged in
        (user.accessToken !== null) ?
            <Redirect to="/"/>
            :
        <Fragment>
            <h1>Login</h1>

            <Form onSubmit={e => loginUser(e)}>
            
                <Form.Group>
                <Form.Label>
                        Email address:
                    </Form.Label>
                    <Form.Control 
                        type="email"
                        placeholder="Enter Email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required

                    />

                </Form.Group>
                <Form.Group>
                    <Form.Label>Password:</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Enter your Password"
                        onChange={e => setPassword(e.target.value)}
                        value={password}
                        required
                    />
                </Form.Group>
                { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Submit
                    </Button>
                }

            </Form>

        </Fragment>


    )

    
}