import  './App.css';
//import react component if madami na ung return
//Ayaw gumana ng Fragment kaya React ang ginamit ko
import React from 'react';
//import navbar
import AppNavbar from './components/AppNavbar';
//import home
import Home from './pages/Home';
//import courses
import Courses from './pages/Courses';
// import Counter from './components/Counter';
import Register from './pages/Register';
import Login from './pages/Login';
import Error from './pages/Error';
import Logout from './pages/Logout';
//import React Context - binago kasi ung nasa UserCOntext
// import UserContext from './UserContext';
import { UserProvider } from './UserContext';

//import specific courses
import SpecificCourse from './pages/SpecificCourse';

//routing components
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import { useState, useEffect } from 'react';


// import container from bootsrap
import { Container } from 'react-bootstrap';



function App() {

  //add a state hook for user
  //the getItem() method returns value of the specified storage object item
  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    email:  localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  //forlogout function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  }
  //use to check if the user information is properly sttored upon login and the local Storage information is cleared upon logout
  useEffect(()=>{
    console.log(user);
    console.log(localStorage);
  }, [user])


  return (
    //provider components that allows consuming components to subscribe to context changes
    <UserProvider value={ {user, setUser, unsetUser} }>
      <Router>
        <AppNavbar/>
        <Container>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/courses" component={Courses} />
              <Route exact path="/courses/:courseId" component={SpecificCourse}/>
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/logout" component={Logout} />
              {/* <Counter /> */}
              <Route component={Error}/>
            </Switch>
        </Container>
      </Router>
    </UserProvider>
    

  );
} 

export default App;
