import React from "react";

// export default React.createContext();

const UserContext = React.createContext();
export const UserProvider = UserContext.Provider;

export default UserContext;
