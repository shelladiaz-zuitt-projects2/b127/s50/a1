import { useState, useEffect, Fragment } from 'react';
import { Button } from 'react-bootstrap';
//what does useEffect do? By using this hook, you tell React that your components/elements need to do something after render. React will remember the function you passed(we'll refer to it as our 'side-effect') and call it later after performing the DOM updates

//syntax of useEffect
/**
 * 
 * useEffect(()=>{}, [])
 */


export default function Counter(){
    const[count, setCount] = useState(0)

    //useEffect
    useEffect(()=>{
        document.title = `You click ${count} times`
    }, [count])

    return(
        <Fragment>
            <p>
                You clicked {count}
            </p>
            <Button variant="primary" onClick={()=> setCount(count + 1)}>Click Me</Button>
        </Fragment>
    )
}